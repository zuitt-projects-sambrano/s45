import React from "react";

// Creates a context object
    // Context object as the same states is a data type an object that can be used to store information that can be shared to other components within app.
    // The context object is a different approach to passing information between components and allows us for easier access by avoiding the use of prop drilling

const userContext = React.createContext();

// The "Provider" component allows other components to consume the context object and supply the necessary information needed to the context object

export const UserProvider = userContext.Provider;

export default userContext;